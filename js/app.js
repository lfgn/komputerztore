/**
 * Variables binded to HTML elements of web site
 * @type {HTMLElement}
 */
const laptopCascadeList = document.getElementById('laptopsCascadeList');
const laptopFeatures = document.getElementById('laptopFeatures');
const laptopBanner = {
    title: document.getElementById('laptopTitle'),
    description: document.getElementById('laptopDescription'),
    price: document.getElementById('laptopPrice'),
    img: document.getElementById('laptopImg'),
    imgPathUrl: "https://noroff-komputer-store-api.herokuapp.com/"
}
const features = {
    feature1: document.getElementById('feature1'),
    feature2: document.getElementById('feature2'),
    feature3: document.getElementById('feature3')
}
const bankBalance = document.getElementById('bankBalance');
const loanBalance = document.getElementById('loanBalance');
const workBalance = document.getElementById('payBalance');
const loanMenu = document.getElementById('loanMenu');

const getMoneyButton = document.getElementById('getMoney');
const transferButton = document.getElementById('transfer');
const getLoanButton = document.getElementById('getLoan');
const payLoanButton = document.getElementById('payLoan');
const buyNowButton = document.getElementById('buy');
const allFeaturesButton = document.getElementById('allFeat')

/**
 * Object that keeps values of the Bank Data
 * @type {{loan: number, bankBalance: number, eligableForLoan: boolean, hasLoan: boolean}}
 */
const bankData = {
    loan: 0.0,
    bankBalance: 0.0,
    hasLoan: false,
    eligableForLoan: true
}

/**
 * Object that keeps the values of the Work Data
 * @type {{payBalance: number, salary: number}}
 */
const workData = {
    payBalance: 0.0,
    salary: 0.0
}

/**
 * Array where the laptops information from API is stored
 * @type {*[]}
 */
let laptopsCatalog = [];

/**
 * Object that store the date of the current selected laptop
 * @type {{}}
 */
let selectedLaptop = {};

/**
 * Variable that keeps the loan fee (10%) to pay when loan is active and transferring funds from Pay balance to Bank balance is intended
 * @type {number}
 */
let tempLoanFee = 0.0;

/**
 * Fetch method that provide us with laptops catalog data
 */
try {
    fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
        .then(response => response.json())
        .then(data => laptopsCatalog = data)
        .then(laptopsCatalog => addLaptopsToList(laptopsCatalog))
        .then(onLoad)

} catch (error) {
    console.log(error)
}

/**
 * Variable listener that listens to Loan balance to change "hasLoan" boolean accordingly, as well as display properties of Loan Menu
 */
window.setInterval(function () {
    if (!(bankData.loan > 0)) {
        bankData.hasLoan = false;
    }
    if (bankData.hasLoan) {
        loanMenu.style.display = "inline";
    } else if (!bankData.hasLoan) {
        loanMenu.style.display = "none";
    }
}, 1);

/**
 * Method that initializes the default set of data and that is called after Fetch of laptops' catalog data is preformed
 */
function onLoad() {
    console.log(laptopsCatalog)
    bankBalance.innerText = toKr(bankData.bankBalance);
    workBalance.innerText = toKr(workData.payBalance);

    selectedLaptop = laptopsCatalog[0];

    randomize(selectedLaptop.specs); // calling randomize method that sets 3 random features at laptop description element from "specs" list  from selected laptop

    laptopBanner.title.innerText = selectedLaptop.title;
    laptopBanner.description.innerText = selectedLaptop.description;
    laptopBanner.price.innerText = toKr(selectedLaptop.price);
    laptopBanner.img.src = laptopBanner.imgPathUrl + selectedLaptop.image;
}

/**
 * Method that adds laptop titles available in catalog to our cascade menu on laptop description element
 * @param laptops Array of laptop data objects
 */
const addLaptopsToList = (laptops) => {
    laptops.forEach(laptop => addLaptopToList(laptop));
}
/**
 * Method that creates each 'option' html element inside laptop description's cascade menu
 * @param laptop laptop data object
 */
const addLaptopToList = (laptop) => {
    const laptopElement = document.createElement('option');
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopCascadeList.appendChild(laptopElement);
}

/**
 * Method called everytime a new laptop is chosen from laptop description's cascade menu, and display current's laptop data
 * @param e event on cascade menu
 */
const handleLaptopListScroll = e => {
    selectedLaptop = laptopsCatalog[e.target.selectedIndex];
    const bugCatcher = Boolean(selectedLaptop.id === 5);    //Boolean to catch bug ('broken URL') when selecting laptop with id 5
    const array = selectedLaptop.specs;
    features.feature1.innerText = '';
    features.feature2.innerText = '';
    features.feature3.innerText = '';
                                                // resetting past data to be replaced with new selected data
    laptopBanner.title.innerText = '';
    laptopBanner.description.innerText = '';
    laptopBanner.price.innerText = '';
    laptopBanner.img.innerText = '';

    randomize(array);       //setting 3 random features into feature list at laptop's feature element

    laptopBanner.title.appendChild(document.createTextNode(selectedLaptop.title));
    laptopBanner.description.appendChild(document.createTextNode(selectedLaptop.description));
    laptopBanner.price.appendChild(document.createTextNode(toKr(selectedLaptop.price)));
    laptopBanner.img.src = (bugCatcher ? laptopBanner.imgPathUrl + selectedLaptop.image.replace('jpg', 'png') : laptopBanner.imgPathUrl + selectedLaptop.image);
    // designating picture to display on laptop banner with logic to come around API bug ('broken URL')
}

/**
 * Method that assign random features at html template to be displayed on laptop's feature element
 * @param array 'specs' array in selected laptops object
 */
const randomize = (array) => {
    let shuffled = shuffle(array);
    features.feature1.appendChild(document.createTextNode(shuffled[0]));
    features.feature2.appendChild(document.createTextNode(shuffled[1]));
    features.feature3.appendChild(document.createTextNode(shuffled[2]));
}
/**
 * Method that takes an array and return a copy of the array with random shuffled cells
 * @param array 'specs' array in selected laptop's object
 * @returns {*[]} shuffled array in random order
 */
const shuffle = (array) => {
    let shuffledArray = [...array];
    let currentIndex = shuffledArray.length, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        [shuffledArray[currentIndex], shuffledArray[randomIndex]] = [
            shuffledArray[randomIndex], shuffledArray[currentIndex]];
    }
    return shuffledArray;
}

/**
 * Method trigger when clicking 'all features' button in laptop's feature element that display an alert box with the complete list of the current selected laptop's features
 */
const allFeat = () => {
    let features = "";
    selectedLaptop.specs.forEach(spec => features += "• " + spec.toString() + "\n")
    alert(features)
}

/**
 * Method triggered when clicking 'work' button that increase 'Pay balance' by 100kr every click
 */
const getIt = () => {
    workData.payBalance += 100;
    workBalance.innerText = toKr(workData.payBalance);
}

/**
 * Method that handles funds transaction from Pay balance to Bank balance regarding the existence of current loan or not
 */
const transaction = () => {
    if (workData.payBalance > 0) {
        if (bankData.hasLoan) {
            if (confirm("You have an active loan. Before transferring funds 10% of your Pay balance has to be accounted to loan. Want to proceed ? ")) {
                tempLoanFee = workData.payBalance * 0.1;
                workData.payBalance -= tempLoanFee;
                bankData.loan -= tempLoanFee;
                loanBalance.innerText = toKr(bankData.loan);
                workBalance.innerText = toKr(workData.payBalance);
                transferToBank();
            }
        } else transferToBank();
    } else alert("Insufficient funds");

}

/**
 * Method that handles funds transaction from Pay balance to Bank balance and updating of html elements
 */
const transferToBank = () => {
    let amount = parseInt(prompt('Enter amount to transfer to bank'));
    if (amount > 0) {
        if (!(amount > workData.payBalance)) {
            bankData.bankBalance += amount;
            workData.payBalance -= amount;

            workBalance.innerText = toKr(workData.payBalance);
            bankBalance.innerText = toKr(bankData.bankBalance);
            alert("Transaction to bank has been successful")
            tempLoanFee = 0.0;
        } else {
            rollbackLoanFeeAtWrongTransfer();
            alert("Insufficient funds");
        }
    } else {
        rollbackLoanFeeAtWrongTransfer();
        alert("Invalid amount");
    }
}

/**
 * Method invoked in case a funds transaction fails and bank loan currently exists. The method make a rollback on the 10% transaction fee when adding funds to Bank balance with existing bank loan
 */
const rollbackLoanFeeAtWrongTransfer = () => {
    workData.payBalance += tempLoanFee;
    bankData.loan += tempLoanFee;
    loanBalance.innerText = toKr(bankData.loan);
    workBalance.innerText = toKr(workData.payBalance);
    tempLoanFee = 0.0;
}

/**
 * Method triggered when 'Get a loan' button is pressed displaying prompt to input desired loan amount
 */
const getLoan = () => {
    if (bankData.eligableForLoan) {
        if (bankData.bankBalance > 0) {
            let loanAmount = parseInt(prompt("Enter desired loan amount"))
            if (loanAmount > 0) {
                if (!(loanAmount > bankData.bankBalance * 2)) {
                    bankData.loan = loanAmount;
                    bankData.bankBalance += loanAmount;
                    bankData.hasLoan = true;
                    bankData.eligableForLoan = false;

                    loanBalance.innerText = toKr(bankData.loan);
                    bankBalance.innerText = toKr(bankData.bankBalance);


                } else alert("Invalid loan : You cannot loan more than double of your balance")
            } else alert("Invalid loan amount")
        } else alert("Invalid loan : You cannot loan with insufficient funds in balance")
    } else alert("Invalid loan request : You have already a current loan and you haven't purchased a computer.")

}

/**
 * Method triggered when "Pay loan" button is clicked when having an existing bank loan and wish to pay the bank loan with Pay balance
 */
const payLoan = () => {
    if (workData.payBalance > 0) {
        if (confirm("If your Pay balance is less or equal to Loan balance, then your whole Pay balance will be used to pay current loan. Want to proceed?")) {
            if (workData.payBalance > bankData.loan) {
                workData.payBalance -= bankData.loan;
                bankData.loan = 0.0;
                workBalance.innerText = toKr(workData.payBalance);
                alert("Successful payment. You have fully paid your current loan. Your current Pay balance is " + toKr(workData.payBalance))
            } else if (workData.payBalance <= bankData.loan) {
                let paid = toKr(workData.payBalance)
                bankData.loan -= workData.payBalance;
                workData.payBalance = 0.0;
                loanBalance.innerText = toKr(bankData.loan);
                workBalance.innerText = toKr(workData.payBalance);
                alert("Successful payment. You have paid " + paid + " on your loan. Actual Loan balance is " + toKr(bankData.loan))
            }
        }
    } else alert("Insufficient funds")
}

/**
 * Method triggered when 'BUY NOW' button is clicked that handles the purchase of selected laptop
 */
const buyNow = () => {
    let laptopPrice = parseInt(selectedLaptop.price);
    if (bankData.bankBalance >= laptopPrice) {
        if (confirm("Are you sure you want to complete purchase?")) {
            bankData.bankBalance -= laptopPrice;
            bankData.eligableForLoan = true;
            bankBalance.innerText = toKr(bankData.bankBalance)
            alert("Congratulations ! You have successfully bought the " + selectedLaptop.title)
        }
    } else if (bankData.bankBalance < laptopPrice) {
        alert("Insufficient funds")
    }
}

/**
 * Method to parse currency values in SEK kr to be displayed in html template
 * @param value Number to parse
 * @returns {string} formatted string representing currency amount
 */
function toKr(value) {
    var formatter = new Intl.NumberFormat('sv-SE', {
        style: 'currency',
        currency: 'SEK',
    });
    return formatter.format(value).toString();
}

/**
 * Events listeners that binds methods to html button elements and cascade list
 */
laptopCascadeList.addEventListener("change", handleLaptopListScroll)
getMoneyButton.addEventListener("click", getIt);
transferButton.addEventListener("click", transaction);
getLoanButton.addEventListener("click", getLoan);
payLoanButton.addEventListener("click", payLoan);
buyNowButton.addEventListener("click", buyNow);
allFeaturesButton.addEventListener("click", allFeat)

