# KomputerZtore

> Dynamic website developed on WebStorm using Vanilla JS

## Description

> Dynamic website that simulates a Laptop Store. The laptop catalog is fetched from specified API :
```
https://noroff-komputer-store-api.herokuapp.com/computers
```
> The store has a 'Bank' section and a 'Work' section to handle income, funds and loans towards purchasing a laptop.

### Work 
> A user can increase their 'Pay balance' by *"working"* (Clicking the 'Work' button) and adding 100kr everytime. 
> Once 'Pay balance' is greater than zero, the user can transfer funds from the 'Pay balance' to its 'Bank balance' when clicking 'Bank' button.
> User can choose the amount to be transferred to its 'Bank balance'. If a current loan exists, the user will be reminded that before each transfer a 10% fee
> from it's 'Pay balance' will be automatically transferred to the 'Loan balance' before completing the desired transfer.

### Bank
> Once a user have transferred funds from their 'Pay balance' to their 'Bank balance' a loan can be given. The user can decide the amount of the loan  
> as long as it doesn't exceed the double of the current 'Bank balance'. Once a loan is created a 'Loan balance' will appear at the Bank section. If a loan is active and the 
> user have not purchased a computer already the user cannot take a new loan. Once a computer is bought with the current 'Bank balance' a new loan can be created.
> Once a loan is active the user have the possibility to pay for it by clicking the 'Pay loan' button. If user's 'Pay balance' is greater than the 'Loan balance', the whole loan will
> paid when clicking the 'Pay loan' button, returning the remaining amount on the 'Pay balance' at 'Work' section.
> If the 'Loan balance' exceed the current 'Pay balance', when 'Pay loan' button is clicked the whole 'Pay balance' will be transferred to the current loan/debt.

### Buy Now
> Once the user selects a laptop and wants to purchase it, the user can click the button 'BUY NOW' located on the laptop's banner that displays the information regarding the selected laptop. 
> If 'Bank balance' is lower than the laptops price then the user will be noticed and the purchase will not be done. If user's 'Bank balance' is the same or higher than the selected laptop's price
> then the purchase will bi successfully completed.


## Test the application
> This dynamic website is hosted on *GitLab Pages* and available for testing visiting the URL bellow :
```
https://lfgn.gitlab.io/komputerztore/
```

![](https://gitlab.com/lfgn/komputerztore/-/raw/main/assets/komputerZtore.png)
